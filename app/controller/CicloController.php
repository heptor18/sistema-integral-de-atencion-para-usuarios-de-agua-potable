<?php 
# Validamos si existe el parametro ACTN enviado por la url
if ( isset( $_GET[ 'ACTN' ] ) ) {
    $nombre_ciclo = isset($_POST['nombre_ciclo']) ? ($_POST['nombre_ciclo']) : "";
    $fecha_inicio = isset($_POST['fecha_inicio']) ? ($_POST['fecha_inicio']) : "";
    $fecha_fin = isset($_POST['fecha_fin']) ? ($_POST['fecha_fin']) : "";
    $ciclo_actual = isset($_POST['ciclo_actual']) ? 1: 0;
    $id_ciclo = isset($_POST['id_ciclo']) ? ($_POST['id_ciclo']) : "";
   
    # Clase modelo
    require_once( '../model/CicloModel.php');
    # Instacia de la clase
    $ciclo = new CicloModel();
    switch( $_GET[ 'ACTN' ] ) {
        case "ADD":
            if(empty($id_ciclo)){ # De lo contrario se genera un registro
                $rspta = $ciclo->addCiclo($nombre_ciclo, $fecha_inicio, $fecha_fin, $ciclo_actual);
                echo $rspta ? "Registro exitoso" : "FAIL";
                
            }else{ # Si existe el id_persona, entonces realiza una edición. 
                $rspta = $ciclo->editCiclo($id_ciclo, $nombre_ciclo, $fecha_inicio, $fecha_fin, $ciclo_actual);
                echo $rspta ? "Edición exitosa" : "FAIL";
            }
            break;
        case "LIST": #Listado de manzanas
            $rspta = $ciclo->getListCiclo();
            $data = array();
            while($reg = $rspta->fetch_object()) {
                $data[] = array(
                    "0" => $reg->id_ciclo,
                    "1" => $reg->ciclo_actual != 1 ? '<span class="badge bg-danger">inactivo</span>' : '<span class="badge bg-success">activo</span>',
                    "2" => $reg->nombre_ciclo,
                    "3" => $reg->fecha_inicio,
                    "4" => $reg->fecha_fin,
                    "5" => ' <div class="icheck-primary">
                    <input class="mr-2" type="checkbox" data-toggle="switch" id="ciclo_actual" name="ciclo_actual">
                    <label class="label-control" for="ciclo_actual">
                        Definir como ciclo actual
                    </label>
                </div>');
            }
            $res = array(
                "sEcho" => 1,
                "iTotalRecors" =>count($data),
                "iTotalDisplayRecords"=>count($data),
                "aaData"=>$data );
            echo json_encode($res);
             break;
        case "ROWID": # Obtiene fila correspondiente al id recibido
            $rspta = $ciclo->getCicloId($id_ciclo);
            echo $rspta ? json_encode($rspta) : "FAILD";
            break;
        default:
            echo "Ocurrio un error intentelo mas tarde";
            break;
    }
} else{
    header("Laocation:../app/view/page/ErrorRuta.php");
}
?>