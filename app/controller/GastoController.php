<?php 
# Validamos si existe el parametro ACTN enviado por la url
if ( isset( $_GET[ 'ACTN' ] ) ) {
    $fecha = isset($_POST['txt_fecha_gasto']) ? ($_POST['txt_fecha_gasto']) : "";
    $monto = isset($_POST['txt_monto_gasto']) ? ($_POST['txt_monto_gasto']) : "";
    $desc = isset($_POST['txt_desc_gasto']) ? ($_POST['txt_desc_gasto']) : "";
    $img = isset($_POST['txt_img_gasto']) ? ($_POST['txt_img_gasto']) : "";
    $id_comite = isset($_POST['txt_id_comite']) ? ($_POST['txt_id_comite']) : "";
    $id_gasto = isset($_POST['txt_id_gasto']) ? ($_POST['txt_id_gasto']) : "";
    
   
    # Clase modelo
    require_once( '../model/GastoModel.php');
    # Instacia de la clase
    $gasto = new GastoModel();
    switch( $_GET[ 'ACTN' ] ) { 
        case "ADD":
            if(empty($id_gasto)){ # Registra
                $rspta = $gasto->addGasto($fecha,$monto,$desc,$img,$id_comite);
                echo $rspta ? "Registro exitoso" : "FAIL"; 
            } else { # Edita
                $rspta = $gasto->editGasto($id_gasto,$fecha,$monto,$desc,$img,$id_comite);
                echo $rspta ? "Edición exitosa" : "FAIL"; 
            }
            break;
        case "LIST": #Listado de Gastos
            $rspta = $gasto->getListGasto();
            $i = 1;
            $data = array();
            while($reg = $rspta->fetch_object()) { 
                //echo json_encode($reg);
                
                $data[] = array(
                    "0" => $i++,
                    "1" => $reg->nombre_comite,
                    "2" => $reg->fecha_gasto,
                    "3" => '$'.$reg->monto_gasto,
                    "4" => $reg->desc_gasto,
                    "5" => '<a class="btn bg-warning btn-sm" href="#" onclick="onClickVerNota('.$reg->img_nota.');" >
                                <i class="fas fa-file-pdf"></i>
                            </a>',
                    "6" => '<a class="btn bg-warning btn-sm" href="#" onclick="onClickEditGasto('.$reg->id_gasto.');" >
                                <i class="fas fa-pencil-alt"></i> Editar
                            </a>
                            <a class="btn bg-navy btn-sm" href="#" onclick="onClickInfoGasto('.$reg->id_gasto.');" >
                                <i class="fas fa-file-alt mr-2"></i> Informe
                            </a>
                            <a class="btn bg-danger btn-sm" href="#" onclick="onClickDeleteGasto('.$reg->id_gasto.');" >
                                <i class="fas fa-trash-alt"></i> Eliminar
                            </a>'
                                    );
            }
            $res = array(
                "sEcho" => 1,
                "iTotalRecors" =>count($data),
                "iTotalDisplayRecords"=>count($data),
                "aaData"=>$data );
            echo json_encode($res);
             break;
        case "ROWID": # Obtiene fila correspondiente al id recibido
            $rspta = $gasto->getGastoID($id_gasto);
            echo $rspta ? json_encode($rspta) : "FAILD";
            break;
        case "DETAILS":
            $rspta = $gasto->getGastoID($id_gasto);
            echo $rspta ? json_encode($rspta) : "FAILD";
            
            break;
        case "DELETEID":
            $rspta = $gasto->deleteGasto($id_gasto);
            echo $rspta ? "Registro Eliminado" : "FAILD";
            break;
        default:
            echo "Ocurrio un error intentelo mas tarde";
            break;
    }
} else{
    header("Laocation:../app/view/page/ErrorRuta.php");
}
?>