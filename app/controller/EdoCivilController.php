<?php 
# Validamos si existe el parametro ACTN enviado por la url
if ( isset( $_GET[ 'ACTN' ] ) ) {
    $txt_id_edo_civil = isset($_POST['txt_id_edo_civil']) ? ($_POST['txt_id_edo_civil']) : "";
    $estado_civil = isset($_POST['estado_civil']) ? ($_POST['estado_civil']) : "";
   
    # Clase modelo
    require_once( '../model/EdoCivilModel.php');
    # Instacia de la clase
    $edo = new EdoCivilModel();
    switch( $_GET[ 'ACTN' ] ) {
        case "LIST": #Listado de estado civil
            $rspta = $edo->getListEdoCivil();
            $data = array();
            while($reg = $rspta->fetch_object()) {
                $data[] = array(
                    "0" => $reg->id_edo_civil,
                    "1" => $reg->estado_civil ,
                    "2" => '<a href="#" onclick="goEdoCivil('.$reg->id_edo_civil.');" class="btn bg-gradient-primary btn-sm" title="Detalle"><i class="fa fa-eye"></i></a>'.
                    '<a href="#" title="Editar pal" onclick="goEditEdoCivil('.$reg->id_edo_civil.');" class="btn bg-gradient-warning btn-sm"><i class="fas fa-edit"></i></a>'            );
            }
            $res = array(
                "sEcho" => 1,
                "iTotalRecors" =>count($data),
                "iTotalDisplayRecords"=>count($data),
                "aaData"=>$data );
            echo json_encode($res);
             break;
        case "ROWID": # Obtiene fila correspondiente al id recibido
            $rspta = $edo->getEdoCivilId($txt_id_edo_civil);
            echo $rspta ? json_encode($rspta) : "FAILD";
            break;
        case "SELECT_LIST":
            $rspta = $edo->getSelect();
            echo '<option selected="selected" >SELECCIONA</option>';
			while($row = $rspta->fetch_object()){
				echo '<option value='.$row->id_edo_civil.'>'.$row->estado_civil.'</option>';
			}
            
            break;
        default:
            echo "Ocurrio un error intentelo mas tarde";
            break;
    }
} else{
    header("Laocation:../app/view/page/ErrorRuta.php");
}
?>