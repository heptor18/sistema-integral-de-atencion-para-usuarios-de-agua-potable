<?php 
# Validamos si existe el parametro ACTN enviado por la url
if ( isset( $_GET[ 'ACTN' ] ) ) {
    $desc_servico= isset($_POST['txt_desc_servicio']) ? ($_POST['txt_desc_servicio']) : "";
    $fecha_registro = date('Y-m-d');
    $direccion = isset($_POST['txt_direccion']) ? ($_POST['txt_direccion']) : "";
    $estado_servicio = isset($_POST['estado_servicio']) ? ($_POST['estado_servicio']) : true;
    $id_persona = isset($_POST['txt_id_persona']) ? ($_POST['txt_id_persona']) : "";
    $id_manzana = isset($_POST['txt_idmanzana_s']) ? ($_POST['txt_idmanzana_s']) : "";
    $id_manzana_si = isset($_GET['txt_idmanzana_si']) ? ($_GET['txt_idmanzana_si']) : "";
    $id_servicio = isset($_POST['txt_id_servicio']) ? ($_POST['txt_id_servicio']) : "";
    # Clase modelo
    require_once( '../model/ServicioModel.php');
    # Instacia de la clase
    $service_per = new ServicioModel();
    switch( $_GET[ 'ACTN' ] ) {
        case "ADD": # Edita o Inserta
            if ( empty($id_servicio) ) { # De lo contrario se genera un registro
                $rspta = $service_per->addServicio($direccion, $desc_servico, $fecha_registro, $id_persona, $id_manzana);
                #echo $rspta ? "Registro exitoso" : "FAIL";
                echo $rspta;
            } else { # Si existe el id_servicio, entonces realiza una edición. 
                $rspta = $service_per->editServicio($id_servicio,$direccion, $desc_servico, $fecha_registro, $id_persona, $id_manzana);
                echo $rspta ? "Edición exitosa" : "FAIL";
            }
            break;
        case "LIST": #Listado de manzanas
            $rspta = $service_per->getListServicio($id_manzana_si);
            $data = array();
            $i = 1;
            while($reg = $rspta->fetch_object()) {
                $data[] = array(
                    "0" => $i++, 
                    "1" => $reg->responsable,
                    "2" => $reg->nombre_manzana,
                    "3" => $reg->desc_servicio,
                    "4" => '<a class="btn bg-success btn-sm" href="#" onclick="goPersonaId('.$reg->id_persona.','.$reg->id_servicio.');" >
                                <i class="fas fa-folder"></i>
                                Tarjeta
                            </a>
                            <a class="btn bg-primary btn-sm" href="#" onclick="openModalServicio('.$reg->id_persona.');" >
                                <i class="fa fa-plus-circle"> </i>
                                Agregar servicio
                            </a>'
                 );
            }
            $res = array(
                "sEcho" => 1,
                "iTotalRecors" =>count($data),
                "iTotalDisplayRecords"=>count($data),
                "aaData"=>$data );
            echo json_encode($res);
            break;
        case "ROWID": # Obtiene fila correspondiente al id recibido
            $rspta = $service_per->getServicioId($id_servicio);
            echo $rspta ? json_encode($rspta) : "FAILD";
            break;
        default:
            echo "Ocurrio un error intentelo mas tarde";
            break;
    }
} else{
    header("Laocation:../app/view/page/ErrorRuta.php");
}
?>