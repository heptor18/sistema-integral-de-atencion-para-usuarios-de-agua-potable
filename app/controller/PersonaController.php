<?php 
# Validamos si existe el parametro ACTN enviado por la url
if ( isset( $_GET[ 'ACTN' ] ) ) {
    $txt_user = isset($_POST['txt_user']) ? ($_POST['txt_user']) : "";
    $txt_apepat = isset($_POST['txt_apepat']) ? ($_POST['txt_apepat']) : "";
    $txt_apemat = isset($_POST['txt_apemat']) ? ($_POST['txt_apemat']) : "";
    $txt_edocivil = isset($_POST['txt_edocivil']) ? ($_POST['txt_edocivil']) : "";
    $txt_direccion = isset($_POST['txt_direccion']) ? ($_POST['txt_direccion']) : "";
    $txt_idmanzana = isset($_POST['txt_idmanzana_u']) ? ($_POST['txt_idmanzana_u']) : "";
    $txt_idpersona = isset($_POST['txt_idpersona']) ? ($_POST['txt_idpersona']) : "";
   
    # Clase modelo
    require_once( '../model/PersonaModel.php');
    # Instacia de la clase
    $user = new PersonaModel(); 
    switch( $_GET[ 'ACTN' ] ) {
        case "ADD": # Edita o Inserta
            if(empty($txt_idpersona)){ # De lo contrario se genera un registro
                $rspta = $user->addPersona($txt_user, $txt_apepat, $txt_apemat, $txt_direccion,
                                     $txt_idmanzana, $txt_edocivil);
                echo $rspta ? "Registro exitoso" : "FAIL";
                
            }else{ # Si existe el id_persona, entonces realiza una edición. 
                $rspta = $user->editPersona($txt_idpersona, $txt_user, $txt_apepat, $txt_apemat, $txt_direccion, $txt_idmanzana, $txt_edocivil);
                echo $rspta ? "Edición exitosa" : "FAIL";
            }
            break;
        case "LIST": #Listado de usuarios
            $rspta = $user->getListPersona();
            $i = 0;
            $data = array();
            while($reg = $rspta->fetch_object()) { 
                $data[] = array(
                    "0" => ++$i,
                    "1" => $reg->estado_persona != 1 ? '<span class="badge bg-danger">inactivo</span>' : '<span class="badge bg-success">activo</span>',
                    "2" => $reg->nombreCompleto,
                    "3" => $reg->direccion,
                    "4" => $reg->nombre_manzana,
                    "5" =>  '<a class="btn bg-gradient-primary btn-sm" href="#" onclick="goPersonaId('.$reg->id_persona.');" >
                                <i class="fas fa-folder"></i>
                                Ver
                            </a>
                            <a class="btn bg-gradient-warning btn-sm" href="#" onclick="goEditPersonaId('.$reg->id_persona.');" >
                                <i class="fas fa-pencil-alt">
                                </i>
                                Editar
                            </a>'
                             );
            }
            $res = array(
                "sEcho" => 1,
                "iTotalRecors" =>count($data),
                "iTotalDisplayRecords"=>count($data),
                "aaData"=>$data );
            echo json_encode($res);
             break;
        case "ROWID": # Obtiene fila correspondiente al id recibido
            $rspta = $user->getPersonaId($txt_idpersona);
            echo $rspta ? json_encode($rspta) : "FAILD";
            break;
        case "SELECT_LIST":
            $rspta = $user->getSelect();
            echo '<option selected="selected" >SELECCIONA</option>';
			while($row = $rspta->fetch_object()){
				echo '<option value='.$row->id_persona.'>'.$row->responsable.'</option>';
			}
            break;
        case "ID_SERVICIO":
            $rspta = $user->getIdServicioPersona($txt_idpersona);
            echo json_encode($rspta->fetch_object());
            break;
        default:
            echo "Ocurrio un error intentelo mas tarde";
            break;
    }
} else{
    header("Laocation:../app/view/page/ErrorRuta.php");
}
?>