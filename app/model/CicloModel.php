<?php 
# Se inclulle la conexión a la DB.
require_once("../config/Conexion.php"); 

/**
 * CicloModel
 */
class CicloModel {
        
    /**
     * Method __construct
     *
     * @return void
     */
    public function __construct(){

    }
    
    /**
     * Method getListCiclo
     *  Obtiene el listado de los cilos que se han registrado en la Base de datos
     * @return Object
     */
    public function getListCiclo(){
        return queryExecute("SELECT * FROM ciclo");
    }
        
    /**
     * Method getCicloId
     * Obtiene el registro que corresponde al identificador que se le manda por parametro
     * @param int $id Identificador del Ciclo
     *
     * @return array
     */
    public function getCicloId($id){
        return queryRowID("SELECT * FROM ciclo WHERE id_ciclo=$id");
    }

    /**
     * Method addCiclo
     * Agrega un nuevo ciclo.
     * @param string $nombre_ciclo Nombre del ciclo
     * @param string $fecha_inicio Fecha de inicio
     * @param string $fecha_fin Fecha de termino
     * @param boolean $ciclo_actual Verdadero si es el ciclo que esta actualamente.
     *
     * @return boolean
     */
    public function addCiclo($nombre_ciclo, $fecha_inicio, $fecha_fin, $ciclo_actual){
        return queryExecute("INSERT INTO ciclo
                                VALUES(0,'$nombre_ciclo','$fecha_inicio', '$fecha_fin',$ciclo_actual)");
    }
    
    /**
     * Method editCiclo
     *  Edita los datos de un ciclo
     * @param int $id Identificador del Ciclo
     * @param string $nombre_ciclo Nombre del ciclo
     * @param string $fecha_inicio Fecha de inicio
     * @param string $fecha_fin Fecha de termino
     * @param boolean $ciclo_actual Verdadero si es el ciclo que esta actualamente.
     *
     * @return boolean
     */
    public function editCiclo($id,$nombre_ciclo, $fecha_inicio, $fecha_fin, $ciclo_actual){
        return queryExecute("UPDATE ciclo SET nombre_ciclo='$nombre_ciclo',
                                                fecha_inicio='$fecha_inicio',
                                                fecha_fin='$fecha_fin',
                                                ciclo_actual=$ciclo_actual
                                        WHERE id_ciclo=$id");
    }
        
    /**
     * Method changeCiclo
     *
     * @param $flag $flag [explicite description]
     *
     * @return boolean
     */
    public function changeCiclo($id, $flag){
        return queryExecute("UPDATE ciclo SET ciclo_actual=$flag
                                        WHERE id_ciclo=$id");
    }

}