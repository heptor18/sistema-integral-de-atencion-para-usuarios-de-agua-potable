<?php 
# Se inclulle la conexión a la DB.
require_once("../config/Conexion.php"); 
class PersonaModel{
    
    
    /**
     * Method __construct
     *
     * @return void
     */
    public function __construct( ){ }

        
    /**
     * Method getListPersona
     * Obtiene el listado de personas registradas en el sistema en orden descendente
     * @return Object
     */
    public function getListPersona(){
        return queryExecute("SELECT 
                                p.*, CONCAT(p.nombre_persona,' ', p.ape_pat) AS nombreCompleto, m.* 
                            FROM 
                                persona p INNER JOIN manzana m 
                            ON 
                                p.id_manzana = m.id_manzana 
                            ORDER BY 
                                p.id_persona 
                            DESC");
    }
    
    /**
     * Method getPersonaId
     * Obtiene la información de una persona
     * @param int $id Identificador correspondiente a la persona
     *
     * @return array
     */
    public function getPersonaId($id){
        return queryRowID("SELECT 
                                p.*, m.*, e.*,
                                (SELECT COUNT(*) FROM servicio WHERE id_persona = p.id_persona) AS tomas
                            FROM 
                                persona p 
                            INNER JOIN 
                                manzana m 
                            ON 
                                p.id_manzana = m.id_manzana
                            INNER JOIN
                                edo_civil e
                            ON
                                e.id_edo_civil = p.id_edo_civil
                            WHERE
                                p.id_persona = $id");
    }
    
    /**
     * Method addPersona
     * Registra na nueva manzana
     * @param string $nombre_persona Nombre de la persona
     * @param string $ape_pat Apellido paterno
     * @param string $ape_mar Apellido Materno
     * @param string $direccion Dirección
     * @param int $id_manzana ID de Manzana
     * @param int $id_edo_civil ID de Estado civil
     *
     * @return boolean
     */
    public function addPersona($nombre_persona, $ape_pat, $ape_mat, $direccion, $id_manzana, $id_edo_civil){
        return queryExecute("INSERT INTO
                                persona
                            VALUES(0, '$nombre_persona', '$ape_pat', '$ape_mat', '$direccion', 1, $id_manzana, $id_edo_civil)");
    }
    
    /**
     * Method editPersona
     * Edita los datos de una persona
     * @param int $id_persona ID de la persona
     * @param string $nombre_persona Nombre de la persona
     * @param string $ape_pat Apellido paterno
     * @param string $ape_mar Apellido Materno
     * @param string $direccion Dirección
     * @param int $id_manzana ID de Manzana
     * @param int $id_edo_civil ID de Estado civil
     *
     * @return boolean
     */
    public function editPersona($id_persona, $nombre_persona, $ape_pat, $ape_mat, $direccion, $id_manzana, $id_edo_civil){
        return queryExecute("UPDATE persona SET nombre_persona='$nombre_persona', 
                                                ape_pat='$ape_pat',
                                                ape_mat='$ape_mat',
                                                direccion='$direccion',
                                                id_manzana=$id_manzana,
                                                id_edo_civil=$id_edo_civil
                                            WHERE id_persona=$id_persona");
    }
    
    /**
     * Method getSelect
     * Llena objeto SELECT con los nombre de los usuarios de la base de datos
     * @return Object
     */
    public function getSelect(){
        return queryExecute("SELECT p.id_persona, m.nombre_manzana, 
                                    CONCAT(p.nombre_persona,' ',p.ape_pat) AS responsable 
                            FROM persona p INNER JOIN manzana m ON p.id_manzana = m.id_manzana");
    }
    
    public function getIdServicioPersona($id){
        return queryExecute("SELECT 
                                p.id_servicio, s.id_servicio 
                            FROM 
                                persona p 
                            INNER JOIN 
                                servicio s 
                            ON 
                                p.id_persona = s.id_persona
                            WHERE
                                p.id_persona=$id");
    }

}
