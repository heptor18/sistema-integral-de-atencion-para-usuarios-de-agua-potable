<?php 
# Se inclulle la conexión a la DB.
require_once("../config/Conexion.php");

/**
 * ComiteModel
 */
class ComiteModel{    
    /**
     * Method __construct
     *
     * @return void
     */
    public function __construct(){

    }
    
    /**
     * Method addComite
     *
     * @param $user $user [explicite description]
     * @param $pass $pass [explicite description]
     * @param $desc $desc [explicite description]
     * @param $fecha $fecha [explicite description]
     * @param $sesion $sesion [explicite description]
     * @param $id_persona $id_persona [explicite description]
     * @param $id_ciclo $id_ciclo [explicite description]
     *
     * @return void
     */
    public function addComite($user, $pass, $desc, $fecha, $sesion, $id_persona, $id_ciclo){}
    
    /**
     * Method editComite
     *
     * @param $id $id [explicite description]
     * @param $user $user [explicite description]
     * @param $pass $pass [explicite description]
     * @param $desc $desc [explicite description]
     * @param $fecha $fecha [explicite description]
     * @param $sesion $sesion [explicite description]
     * @param $id_persona $id_persona [explicite description]
     * @param $id_ciclo $id_ciclo [explicite description]
     *
     * @return void
     */
    public function editComite($id, $user, $pass, $desc, $fecha, $sesion, $id_persona, $id_ciclo){}
    
    /**
     * Method changeSession
     *
     * @param $flag $flag [explicite description]
     *
     * @return void
     */
    public function changeSession($flag){
        
    }
}
?>