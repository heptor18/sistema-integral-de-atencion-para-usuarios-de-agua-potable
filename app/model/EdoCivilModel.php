<?php 
# Se inclulle la conexión a la DB.
require_once("../config/Conexion.php"); 

/**
 * EdoCivilModel
 */
class EdoCivilModel{
    
    /**
     * Method __construct
     *
     * @return void
     */
    public function __construct(){}
    
    /**
     * Method getListEdoCivil
     *
     * @return Object
     */
    public function getListEdoCivil(){
        return queryExecute("SELECT * FROM edo_civil");
    }
    
    /**
     * Method getEdoCivilId
     *
     * @param $id $id [explicite description]
     *
     * @return void
     */
    public function getEdoCivilId($id){
        
    }
    
    /**
     * Method getSelect
     *
     * @return Object
     */
    public function getSelect(){
        return queryExecute("SELECT * FROM edo_civil");
    }
}