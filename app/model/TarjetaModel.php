<?php 
# Se inclulle la conexión a la DB.
require_once("../config/Conexion.php"); 

/**
 * TarjetaModel
 */
class TarjetaModel{
        
    /**
     * Method __construct
     *
     * @return void
     */
    public function __construct(){
    }
    
    /**
     * Method getListTarjeta
     *
     * @return void
     */
    public function getListTarjeta(){
        
    }
        
    /**
     * Method addTarjeta
     *
     * @param $numero_tarjeta $numero_tarjeta [explicite description]
     * @param $estado $estado [explicite description]
     * @param $id_servicio $id_servicio [explicite description]
     * @param $id_ciclo $id_ciclo [explicite description]
     *
     * @return void
     */
    public function addTarjeta($numero_tarjeta, $estado, $id_servicio, $id_ciclo){

    }
    
    /**
     * Method editTarjeta
     *
     * @param $id $id [explicite description]
     * @param $numero_tarjeta $numero_tarjeta [explicite description]
     * @param $estado $estado [explicite description]
     * @param $id_servicio $id_servicio [explicite description]
     * @param $id_ciclo $id_ciclo [explicite description]
     *
     * @return void
     */
    public function editTarjeta($id, $numero_tarjeta, $estado, $id_servicio, $id_ciclo){

    }
    /**
     * Method addPago
     *
     * @return void
     */
    public function addPago($recibo){}
}
