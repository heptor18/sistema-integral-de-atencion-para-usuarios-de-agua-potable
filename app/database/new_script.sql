
CREATE TABLE mes (
                id_mes INT(11) NOT NULL,
                nombre_mes VARCHAR(30) NOT NULL,
                nombre_clave VARCHAR(10) NOT NULL,
                PRIMARY KEY (id_mes)
);



CREATE TABLE ciclo (
                id_ciclo INT AUTO_INCREMENT NOT NULL,
                nombre_ciclo VARCHAR(50) NOT NULL,
                fecha_inicio DATE NOT NULL,
                fecha_fin DATE NOT NULL,
                ciclo_actual TINYINT NOT NULL,
                PRIMARY KEY (id_ciclo)
);


CREATE TABLE recibo (
                id_recibo INT AUTO_INCREMENT NOT NULL,
                no_folio VARCHAR(30) NOT NULL,
                fecha_pago DATE NOT NULL,
                concepto VARCHAR(50) NOT NULL,
                monto DOUBLE PRECISION NOT NULL,
                id_mes INT NOT NULL,
                PRIMARY KEY (id_recibo)
);


CREATE TABLE edo_civil (
                id_edo_civil INT AUTO_INCREMENT NOT NULL,
                estado_civil VARCHAR(30) NOT NULL,
                PRIMARY KEY (id_edo_civil)
);


CREATE TABLE manzana (
                id_manzana INT AUTO_INCREMENT NOT NULL,
                nombre_manzana VARCHAR(30) NOT NULL,
                des_manzana VARCHAR(50) NOT NULL,
                PRIMARY KEY (id_manzana)
);


CREATE TABLE persona (
                id_persona INT AUTO_INCREMENT NOT NULL,
                nombre_persona VARCHAR(30) NOT NULL,
                direccion VARCHAR(100) NOT NULL,
                ape_mat VARCHAR(30),
                ape_pat VARCHAR(30) NOT NULL,
                id_manzana INT NOT NULL,
                id_edo_civil INT NOT NULL,
                PRIMARY KEY (id_persona)
);


CREATE TABLE servicio (
                id_servicio INT AUTO_INCREMENT NOT NULL,
                direccion VARCHAR(100) NOT NULL,
                desc_servicio VARCHAR(30) NOT NULL,
                fecha_registro DATE NOT NULL,
                estado_servicio TINYINT NOT NULL,
                id_persona INT NOT NULL,
                id_manzana INT NOT NULL,
                PRIMARY KEY (id_servicio)
);


CREATE TABLE tarjeta (
                id_tarjeta INT AUTO_INCREMENT NOT NULL,
                numero_tarjeta VARCHAR(30) NOT NULL,
                estado_tarjeta TINYINT NOT NULL,
                id_recibo INT NOT NULL,
                id_servicio INT NOT NULL,
                id_ciclo INT NOT NULL,
                PRIMARY KEY (id_tarjeta)
);


CREATE TABLE comite (
                id_comite INT AUTO_INCREMENT NOT NULL,
                user_1 VARCHAR(30) NOT NULL,
                pass VARCHAR(30) NOT NULL,
                desc_comite VARCHAR(30) NOT NULL,
                fecha_ingreso DATE NOT NULL,
                sesion TINYINT NOT NULL,
                id_persona INT NOT NULL,
                id_ciclo INT NOT NULL,
                PRIMARY KEY (id_comite)
);



CREATE TABLE gasto (
                id_gasto INT AUTO_INCREMENT NOT NULL,
                fecha_gasto DATE NOT NULL,
                monto_gasto DOUBLE PRECISION NOT NULL,
                desc_gasto VARCHAR(50) NOT NULL,
                img_nota LONGBLOB,
                id_comite INT NOT NULL,
                PRIMARY KEY (id_gasto)
);


ALTER TABLE recibo ADD CONSTRAINT mes_recibo_fk
FOREIGN KEY (id_mes)
REFERENCES mes (id_mes)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tarjeta ADD CONSTRAINT ciclo_tarjeta_fk
FOREIGN KEY (id_ciclo)
REFERENCES ciclo (id_ciclo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE comite ADD CONSTRAINT ciclo_comite_fk
FOREIGN KEY (id_ciclo)
REFERENCES ciclo (id_ciclo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tarjeta ADD CONSTRAINT recibo_tarjeta_fk
FOREIGN KEY (id_recibo)
REFERENCES recibo (id_recibo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE persona ADD CONSTRAINT edo_civil_persona_fk
FOREIGN KEY (id_edo_civil)
REFERENCES edo_civil (id_edo_civil)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE persona ADD CONSTRAINT manzana_persona_fk
FOREIGN KEY (id_manzana)
REFERENCES manzana (id_manzana)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE servicio ADD CONSTRAINT manzana_servicio_fk
FOREIGN KEY (id_manzana)
REFERENCES manzana (id_manzana)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE comite ADD CONSTRAINT persona_comite_fk
FOREIGN KEY (id_persona)
REFERENCES persona (id_persona)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE servicio ADD CONSTRAINT persona_servicio_fk
FOREIGN KEY (id_persona)
REFERENCES persona (id_persona)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tarjeta ADD CONSTRAINT servicio_tarjeta_fk
FOREIGN KEY (id_servicio)
REFERENCES servicio (id_servicio)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE gasto ADD CONSTRAINT comite_gasto_fk
FOREIGN KEY (id_comite)
REFERENCES comite (id_comite)
ON DELETE NO ACTION
ON UPDATE NO ACTION;