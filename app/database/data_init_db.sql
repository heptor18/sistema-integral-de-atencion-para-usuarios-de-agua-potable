--
-- Querys para insertar los primeros 
-- registros de la base de datos `siaua`
--
-- --------------------------------------------------------
--
-- # Datos Tabla Mes
--
INSERT INTO mes (id_mes, nombre_mes, nombre_clave) 
    VALUES
        (1, 'Enero',     'ENE'),
        (2, 'Febrero',   'FEB'),
        (3, 'Marzo',     'MAR'),
        (4, 'Abril',     'ABR'),
        (5, 'Mayo',      'MAY'),
        (6, 'Junio',     'JUN'),
        (7, 'Julio',     'JUL'),
        (8, 'Agosto',    'AGO'),
        (9, 'Septiembre','SEP'),
        (10, 'Octubre',  'OCT'),
        (11, 'Noviembre','NOV'),
        (12, 'Diciembre','DIC');
-- --------------------------------------------------------
--
-- # Datos Tabla Ciclo
--
INSERT INTO ciclo (id_ciclo, nombre_ciclo, fecha_inicio, fecha_fin, edo_ciclo) 
    VALUES
        (1, 'Periodo 2019', '2019-08-26', '2020-08-26', 0),
        (2, 'Periodo 2020', '2020-01-01', '2021-01-01', 0),
        (3, 'Periodo 2021', '2021-01-01', '2022-02-12', 0),
        (4, 'Periodo 2022', '2022-08-10', '2023-08-11', 1);
-- --------------------------------------------------------
--
-- # Datos Estado Civil
--
INSERT INTO edo_civil (id_edo_civil, estado_civil)
    VALUES
        (1, 'Saltero(a)'),
        (2, 'Casado(a)'),
        (3, 'Viudo(a)'),
        (4, 'Divorciado(a)');
-- --------------------------------------------------------
--
-- # Datos Tabla Manzana
--
INSERT INTO manzana (id_manzana, nombre_manzana, des_manzana)
    VALUES
        (1, 'Los Fresnos', 'Manzana los Fresnos'),
        (2, 'La Playa', 'Manzana la playa'),
        (3, 'La Palma', 'Manzana la palma'),
        (4, 'La Frontera', 'Manzana la frontera'),
        (5, 'Centro', 'Manzana Centro'),
        (6, 'Barrio Alto', 'Manzana Barrio Alto'),
        (7, 'Fraccionamiento', 'Manzana Fraccionamiento'),
        (8, '3 de Mayo', 'Manzana 3 DE Mayo');
-- --------------------------------------------------------

--
-- # Datos Tabla Persona
--
INSERT INTO persona (id_persona, nombre_persona, ape_pat, ape_mat, direccion, edo_persona, id_manzana, id_edo_civil) 
    VALUES
        (1, 'Hector', 'Hernandez', 'Estrada', 'Av Reforma S/N',         1, 2, 3),
        (2, 'Gladis', 'Falcón', 'Amado', 'Avenida Tula',                1, 1, 1),
        (3, 'Eva', 'Jimenez', 'Acevedo', 'Calle Moderna S/N',           1, 3, 2),
        (4, 'Saul', 'Alvarez', 'Aguilar', 'Calle Melchor Ocampo',       1, 4, 3),
        (5, 'Maria de Jesus', 'Gonzalez ', 'Angeles', 'Av Reforma S/N', 1, 7, 1),
        (6, 'Alberta', 'Estrada', 'Castillo', 'Calle Moderna S/N',      1, 5, 2),
        (7, 'Emilio', 'Hernández', 'Gonzaléz', 'Av. Reforma S/N',       1, 8, 2),
        (8, 'Santiago', 'Flores', 'Falcón', 'Calle Hidalgo S/N',        1, 7, 3),
        (9, 'Jose', 'Garcia', 'Luna', 'Av Insurgentes',                 1, 6, 4),
        (10, 'Doroteo', 'Angeles', 'Perez', 'Calle Melchor Ocampo',     1, 2, 2);
--
-- # Datos Tabla Mes
--

-- --------------------------------------------------------
--
-- # Datos Tabla Mes
--

-- --------------------------------------------------------
--
-- # Datos Tabla Mes
--

-- --------------------------------------------------------
--
-- # Datos Tabla Mes
--

-- --------------------------------------------------------
--
-- # Datos Tabla Mes
--

-- --------------------------------------------------------
--
-- # Datos Tabla Mes
--
