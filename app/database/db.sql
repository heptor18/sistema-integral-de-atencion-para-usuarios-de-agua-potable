-- --------------------------------------------------------
--
-- Estructura de tabla para la tabla `mes`
--
CREATE TABLE mes (
    id_mes INT(11) AUTO_INCREMENT NOT NULL,
    nombre_mes VARCHAR(20) NOT NULL,
    nombre_clave VARCHAR(10) NOT NULL,
    PRIMARY KEY (id_mes)
);
--
-- Volcado de datos para la tabla `mes`
--
INSERT INTO mes (id_mes, nombre_mes, nombre_clave) 
    VALUES
        (1, 'Enero', 'ENE'),
        (2, 'Febrero', 'FEB'),
        (3, 'Marzo', 'MARZ'),
        (4, 'Abril', 'ABR'),
        (5, 'Mayo', 'MAY'),
        (6, 'Junio', 'JUN'),
        (7, 'Julio', 'JUL'),
        (8, 'Agosto', 'AGO'),
        (9, 'Septiembre', 'SEP'),
        (10, 'Octubre', 'OCT'),
        (11, 'Noviembre', 'NOV'),
        (12, 'Diciembre', 'DIC');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciclo`
--
CREATE TABLE ciclo (
    id_ciclo INT(11) AUTO_INCREMENT NOT NULL,
    nombre_ciclo VARCHAR(50) NOT NULL,
    fecha_inicio DATE NOT NULL,
    fecha_fin DATE NOT NULL,
    ciclo_actual TINYINT(4) NOT NULL,
    PRIMARY KEY (id_ciclo)
);
--
-- Volcado de datos para la tabla `ciclo`
--
INSERT INTO ciclo (id_ciclo, nombre_ciclo, fecha_inicio, fecha_fin, ciclo_actual) 
    VALUES
        (1, 'Periodo 2019', '2019-08-26', '2020-08-26', 0),
        (2, 'Periodo 2020', '2020-01-01', '2021-01-01', 0),
        (3, 'Periodo 2021', '2021-01-01', '2022-02-12', 0),
        (4, 'Periodo 2022', '2022-08-10', '2023-08-11', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recibo`
--
CREATE TABLE recibo (
                id_recibo INT(11) AUTO_INCREMENT NOT NULL,
                no_folio VARCHAR(30) NOT NULL,
                fecha_pago DATE NOT NULL,
                concepto VARCHAR(50) NOT NULL,
                monto DOUBLE NOT NULL,
                PRIMARY KEY (id_recibo)
);
--
-- Volcado de datos para la tabla `recibo`
--
INSERT INTO recibo (id_recibo, no_folio, fecha_pago, concepto, monto) 
    VALUES
        (1, '00001', '2022-08-13', 'Servicio de agua', 360);
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `edo_civil`
--
CREATE TABLE edo_civil (
    id_edo_civil INT(11) AUTO_INCREMENT NOT NULL,
    estado_civil VARCHAR(30) NOT NULL,
    PRIMARY KEY (id_edo_civil)
);
--
-- Volcado de datos para la tabla `edo_civil`
--
INSERT INTO edo_civil (id_edo_civil, estado_civil)
    VALUES
        (1, 'Saltero(a)'),
        (2, 'Casado(a)'),
        (3, 'Viudo(a)'),
        (4, 'Divorciado(a)');
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `manzana`
--
CREATE TABLE manzana (
    id_manzana INT(11) AUTO_INCREMENT NOT NULL,
    nombre_manzana VARCHAR(30) NOT NULL,
    des_manzana VARCHAR(50) NOT NULL,
    PRIMARY KEY (id_manzana)
);
--
-- Volcado de datos para la tabla `manzana`
--
INSERT INTO manzana (id_manzana, nombre_manzana, des_manzana)
    VALUES
        (1, 'Los Fresnos', 'Manzana los Fresnos'),
        (2, 'La Playa', 'Manzana la playa'),
        (3, 'La Palma', 'Manzana la palma'),
        (4, 'La Frontera', 'Manzana la frontera'),
        (5, 'Centro', 'Manzana Centro'),
        (6, 'Barrio Alto', 'Manzana Barrio Alto'),
        (7, 'Fraccionamiento', 'Manzana Fraccionamiento'),
        (8, '3 de Mayo', 'Manzana 3 DE Mayo');
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--
CREATE TABLE persona (
    id_persona INT(11) AUTO_INCREMENT NOT NULL,
    nombre_persona VARCHAR(30) NOT NULL,
    direccion VARCHAR(100) NOT NULL,
    ape_mat VARCHAR(30),
    ape_pat VARCHAR(30) NOT NULL,
    edo_persona TINYINT(4) NOT NULL,
    id_manzana INT(11) NOT NULL,
    id_edo_civil INT(11) NOT NULL,
    PRIMARY KEY (id_persona)
);
--
-- Volcado de datos para la tabla `persona`
--
INSERT INTO persona (id_persona, nombre_persona, ape_pat, ape_mat, direccion, edo_persona, id_manzana, id_edo_civil) 
    VALUES
        (1, 'Hector', 'Hernandez', 'Estrada', 'Av Reforma S/N',         1, 2, 3),
        (2, 'Gladis', 'Falcón', 'Amado', 'Avenida Tula',                1, 1, 1),
        (3, 'Eva', 'Jimenez', 'Acevedo', 'Calle Moderna S/N',           1, 3, 2),
        (4, 'Saul', 'Alvarez', 'Aguilar', 'Calle Melchor Ocampo',       1, 4, 3),
        (5, 'Maria de Jesus', 'Gonzalez ', 'Angeles', 'Av Reforma S/N', 1, 7, 1),
        (6, 'Alberta', 'Estrada', 'Castillo', 'Calle Moderna S/N',      1, 5, 2),
        (7, 'Emilio', 'Hernández', 'Gonzaléz', 'Av. Reforma S/N',       1, 8, 2),
        (8, 'Santiago', 'Flores', 'Falcón', 'Calle Hidalgo S/N',        1, 7, 3),
        (9, 'Jose', 'Garcia', 'Luna', 'Av Insurgentes',                 1, 6, 4),
        (10, 'Doroteo', 'Angeles', 'Perez', 'Calle Melchor Ocampo',     1, 2, 2);

CREATE TABLE servicio (
                id_servicio INT(11) AUTO_INCREMENT NOT NULL,
                direccion VARCHAR(100) NOT NULL,
                desc_servicio VARCHAR(100) NOT NULL,
                fecha_registro DATE NOT NULL,
                estado_servicio TINYINT(4) NOT NULL,
                id_persona INT(11) NOT NULL,
                id_manzana INT(11) NOT NULL,
                PRIMARY KEY (id_servicio)
);


CREATE TABLE tarjeta (
                id_tarjeta INT(11) AUTO_INCREMENT NOT NULL,
                numero_tarjeta VARCHAR(30) NOT NULL,
                estado_tarjeta TINYINT(4) NOT NULL,
                id_recibo INT(11) NOT NULL,
                id_servicio INT(11) NOT NULL,
                id_ciclo INT(11) NOT NULL,
                id_mes INT(11) NOT NULL,
                PRIMARY KEY (id_tarjeta)
);

-- --------------------------------------------------------
--
-- Estructura de tabla para la tabla `comite`
--
CREATE TABLE comite (
    id_comite INT(11) AUTO_INCREMENT NOT NULL,
    user_sesion VARCHAR(30) NOT NULL,
    pass VARCHAR(30) NOT NULL,
    desc_comite VARCHAR(30) NOT NULL,
    fecha_ingreso DATE NOT NULL,
    sesion TINYINT(4) NOT NULL,
    id_persona INT(11) NOT NULL,
    id_ciclo INT(11) NOT NULL,
    PRIMARY KEY (id_comite)
);
--
-- Volcado de datos para la tabla `comite`
--
INSERT INTO comite (id_comite, user_sesion, pass, desc_comite, fecha_ingreso, sesion, id_persona, id_ciclo) 
            VALUES
                    (1, 'hhector', 'hhector', 'Comite - Agua potable', '2022-08-13', 1,  1,             1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gasto`
--
CREATE TABLE gasto (
    id_gasto INT(11) AUTO_INCREMENT NOT NULL,
    fecha_gasto DATE NOT NULL,
    monto_gasto DOUBLE PRECISION NOT NULL,
    desc_gasto VARCHAR(50) NOT NULL,
    img_nota LONGBLOB,
    id_comite INT(11) NOT NULL,
    PRIMARY KEY (id_gasto)
);
--
-- Volcado de datos para la tabla `gasto`
--
INSERT INTO gasto (id_gasto, fecha_gasto, monto_gasto, desc_gasto, img_nota, id_comite)
    VALUES
                    (1,       '2022-08-13', 520,        'Energia Electrica', 'nota1.jpg', 1),
                    (2, '2022-09-17', 5000, 'Pago de mantenimiento', 'img2.jpg', 1),
                    (3, '2022-09-15', 5600, 'Pago a secretaria', 'img3.jpg', 1);
-- --------------------------------------------------------

ALTER TABLE tarjeta ADD CONSTRAINT mes_tarjeta_fk
FOREIGN KEY (id_mes)
REFERENCES mes (id_mes)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tarjeta ADD CONSTRAINT ciclo_tarjeta_fk
FOREIGN KEY (id_ciclo)
REFERENCES ciclo (id_ciclo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE comite ADD CONSTRAINT ciclo_comite_fk
FOREIGN KEY (id_ciclo)
REFERENCES ciclo (id_ciclo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tarjeta ADD CONSTRAINT recibo_tarjeta_fk
FOREIGN KEY (id_recibo)
REFERENCES recibo (id_recibo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE persona ADD CONSTRAINT edo_civil_persona_fk
FOREIGN KEY (id_edo_civil)
REFERENCES edo_civil (id_edo_civil)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE persona ADD CONSTRAINT manzana_persona_fk
FOREIGN KEY (id_manzana)
REFERENCES manzana (id_manzana)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE servicio ADD CONSTRAINT manzana_servicio_fk
FOREIGN KEY (id_manzana)
REFERENCES manzana (id_manzana)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE comite ADD CONSTRAINT persona_comite_fk
FOREIGN KEY (id_persona)
REFERENCES persona (id_persona)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE servicio ADD CONSTRAINT persona_servicio_fk
FOREIGN KEY (id_persona)
REFERENCES persona (id_persona)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tarjeta ADD CONSTRAINT servicio_tarjeta_fk
FOREIGN KEY (id_servicio)
REFERENCES servicio (id_servicio)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE gasto ADD CONSTRAINT comite_gasto_fk
FOREIGN KEY (id_comite)
REFERENCES comite (id_comite)
ON DELETE NO ACTION
ON UPDATE NO ACTION;