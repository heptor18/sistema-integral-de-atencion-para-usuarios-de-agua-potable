--
-- Base de datos: `siaua`
--
-- --------------------------------------------------------

--
-- # Mes
--
CREATE TABLE mes (
    id_mes INT(11) AUTO_INCREMENT NOT NULL,
    nombre_mes VARCHAR(20) NOT NULL,
    nombre_clave VARCHAR(10) NOT NULL,
    PRIMARY KEY (id_mes)
);

--
-- # Ciclo
--
CREATE TABLE ciclo (
    id_ciclo INT(11) AUTO_INCREMENT NOT NULL,
    nombre_ciclo VARCHAR(50) NOT NULL,
    fecha_inicio DATE NOT NULL,
    fecha_fin DATE NOT NULL,
    edo_ciclo TINYINT(4) NOT NULL,
    PRIMARY KEY (id_ciclo)
);
--
-- # Recibo
--
CREATE TABLE recibo (
    id_recibo INT(11) AUTO_INCREMENT NOT NULL,
    no_folio VARCHAR(30) NOT NULL,
    fecha_pago DATE NOT NULL,
    concepto VARCHAR(50) NOT NULL,
    monto DOUBLE NOT NULL,
    PRIMARY KEY (id_recibo)
);
--
-- # Estado Civil
--
CREATE TABLE edo_civil (
    id_edo_civil INT(11) AUTO_INCREMENT NOT NULL,
    estado_civil VARCHAR(30) NOT NULL,
    PRIMARY KEY (id_edo_civil)
);
--
-- # Manzana
--
CREATE TABLE manzana (
    id_manzana INT(11) AUTO_INCREMENT NOT NULL,
    nombre_manzana VARCHAR(30) NOT NULL,
    des_manzana VARCHAR(50) NOT NULL,
    PRIMARY KEY (id_manzana)
);
--
-- # Persona
--
CREATE TABLE persona (
    id_persona INT(11) AUTO_INCREMENT NOT NULL,
    nombre_persona VARCHAR(30) NOT NULL,
    ape_mat VARCHAR(30),
    ape_pat VARCHAR(30) NOT NULL,
    direccion VARCHAR(100) NOT NULL,
    edo_persona TINYINT(4) NOT NULL,
    id_manzana INT(11) NOT NULL,
    id_edo_civil INT(11) NOT NULL,
    PRIMARY KEY (id_persona)
);
--
-- # Servicio
--
CREATE TABLE servicio (
    id_servicio INT(11) AUTO_INCREMENT NOT NULL,
    direccion VARCHAR(100) NOT NULL,
    desc_servicio VARCHAR(100) NOT NULL,
    fecha_registro DATE NOT NULL,
    estado_servicio TINYINT(4) NOT NULL,
    id_persona INT(11) NOT NULL,
    id_manzana INT(11) NOT NULL,
    PRIMARY KEY (id_servicio)
);
--
-- # Tarjeta
--
CREATE TABLE tarjeta (
    id_tarjeta INT(11) AUTO_INCREMENT NOT NULL,
    numero_tarjeta VARCHAR(30) NOT NULL,
    estado_tarjeta TINYINT(4) NOT NULL,
    id_recibo INT(11) NOT NULL,
    id_servicio INT(11) NOT NULL,
    id_ciclo INT(11) NOT NULL,
    id_mes INT(11) NOT NULL,
    PRIMARY KEY (id_tarjeta)
);
--
-- # Comite
--
CREATE TABLE comite (
    id_comite INT(11) AUTO_INCREMENT NOT NULL,
    user_sesion VARCHAR(30) NOT NULL,
    pass VARCHAR(30) NOT NULL,
    desc_comite VARCHAR(30) NOT NULL,
    fecha_ingreso DATE NOT NULL,
    sesion TINYINT(4) NOT NULL,
    id_persona INT(11) NOT NULL,
    id_ciclo INT(11) NOT NULL,
    PRIMARY KEY (id_comite)
);
--
-- # Gasto
--
CREATE TABLE gasto (
    id_gasto INT(11) AUTO_INCREMENT NOT NULL,
    fecha_gasto DATE NOT NULL,
    monto_gasto DOUBLE PRECISION NOT NULL,
    desc_gasto VARCHAR(50) NOT NULL,
    img_nota LONGBLOB,
    id_comite INT(11) NOT NULL,
    PRIMARY KEY (id_gasto)
);
--
-- # Recibo
--
CREATE TABLE recibo (
    id_recibo INT(11) AUTO_INCREMENT NOT NULL,
    no_folio VARCHAR(30) NOT NULL,
    fecha_pago DATE NOT NULL,
    concepto VARCHAR(50) NOT NULL,
    monto DOUBLE NOT NULL,
    PRIMARY KEY (id_recibo)
);