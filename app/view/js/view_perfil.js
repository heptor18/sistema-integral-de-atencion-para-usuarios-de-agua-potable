var url_perfil = 'app/controller/PersonaController.php?ACTN=';
var url_servicio = 'app/controller/ServicioController.php?ACTN=';
/**
 * initViewPerfil
 * Carga la información de la persona seleccionada. 
 * @param int id Identificador de la persona a mostrar su información
 */
function initViewPerfil(id, id_item) {
    initDataServicio(id_item);
    $.post(url_perfil.concat('ROWID'),
        { 'txt_idpersona': id }, function (rspta) {
            d = JSON.parse(rspta);
            $("#txt_idpersona").html(id);
            $("#txt_nombre").html(d.nombre_persona);
            $("#txt_direccion").html(d.direccion);
            $("#txt_ape_pat").html(d.ape_pat);
            $("#txt_ape_mat").html(d.ape_mat)
            $("#txt_manzana").html(d.nombre_manzana);
            $("#txt_edo_civil").html(d.estado_civil);
            $("#txt_fecha_pago").html(d.tomas);
        });
}

function initDataServicio(id_servicio) {

    $.post(url_servicio.concat('ROWID'),
        {'txt_id_servicio':id_servicio},
        function(rspta){
            d = JSON.parse(rspta);
            //$("#txt_idpersona").html(id);
            $("#txt_servicio").html(d.desc_servicio);
            $("#txt_direccion_servicio").html(d.direccion + " - "+ d.nombre_manzana);
            $("#txt_alta_servicio").html(d.fecha_registro);
            $("#txt_estatus_servicio").html(d.estado_servicio); 
        });
}

