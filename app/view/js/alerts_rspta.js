var Toast;
Toast = Swal.mixin({
    toast: true,
    animation: true,
    position: 'center'

});
function alert_rspt_success(tipo){
    Toast.fire({
        icon: 'success',
        title: tipo,
        showConfirmButton: false,
        timer: 3000
    });
}

function alert_rspt_error(tipo){
    Toast.fire({
        icon: 'error',
        title: tipo,
        showConfirmButton: false,
        timer: 3000
    });
}
/**
 * Confirmación para la eliminación de posibles registros
 * @param {int} id Es el identificador del registro que se intenta eliminar
 * @param {String} url_controller Dirección del Controlador asociado con la vista
 * @param {String} param Parameto que buscara el controlador para realizar el borrado del registro
 */
function alert_question_delete(){
    return Toast.fire({
        title: '¿Está seguro que desea eliminar este registro?',
        text: "¡No podrás revertir esto!",
        icon: 'warning',
        showCancelButton: true,
        showConfirmButton: true,
        confirmButtonColor: '#2d89f2',
        cancelButtonColor: '#9a9a9c',
        confirmButtonText: '¡Sí, bórralo!',
        cancelButtonText: 'Cancelar'
        });
}