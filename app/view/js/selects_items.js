/**
 * !Función select_persona
 *  * Se encarga de llenar un elemento SELECT
 *  *    con el nombre de las personas de la base de datos
 */
function select_persona(){
    $.post("app/controller/PersonaController.php?ACTN=SELECT_LIST",
        function(rspta){
            $("#txt_id_persona").html(rspta);
            $("#txt_id_persona").selectpicker('refresh');
        }
    );
}

/**
 * !Función select_manzana
 *  * Se encarga de llenar un elemento SELECT
 *  * con el nombre de las manzanas de la base de datos
 */
 function select_manzana(){
    $.post("app/controller/ManzanaController.php?ACTN=SELECT_LIST",
        function(rspta){
            // Select Filtro 
            $("#txt_idmanzana").html(rspta);
            $("#txt_idmanzana").selectpicker('refresh');
            // Modal servico
            $("#txt_idmanzana_s").html(rspta);
            $("#txt_idmanzana_s").selectpicker('refresh');
            // Modal usuario
            $("#txt_idmanzana_u").html(rspta);
            $("#txt_idmanzana_u").selectpicker('refresh');
        }
    );
}

/**
 * !Función select_edo_civil
 *  * Se encarga de llenar un elemento SELECT
 *  * con el nombre de los estados civiles
 */
 function select_edo_civil(){
    $.post("app/controller/EdoCivilController.php?ACTN=SELECT_LIST",
        function(rspta){
            $("#txt_edocivil").html(rspta);
            $("#txt_edocivil").selectpicker('refresh');
        }
    );
}