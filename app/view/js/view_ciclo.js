var url_ciclo = 'app/controller/CicloController.php?ACTN=';

function initViewCiclo(){
    
    tbCiclo();
    //$('input[type="checkbox"]').not('[data-switch-no-init]').bootstrapSwitch()
    
    $("#form-ciclo").on('submit', function(e){
        saveCiclo(e);
    });
}
function addCiclo() {
    cleanInputs();
    $('#modal-ciclo').modal({
        show: true,
        backdrop: 'static'
    });
}
function cleanInputs() {
    $("#id_ciclo").val("");
    $("#nombre_ciclo").val("");
    $("#fecha_inicio").val("");
    $("#fecha_fin").val("");
    $("#ciclo_actual").val("")
}
function saveCiclo(e){
    e.preventDefault();
    var formData = new FormData($("#form-ciclo")[0]);
    $.ajax({
        url: url_ciclo.concat('ADD'),
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data == "FAIL") {
                console.log(data);
                alert_rspt_error("Ocurrio un problema, vuelve a intentarlo");
            } else {
                $("#form-ciclo")[0].reset();
                alert_rspt_success(data);
                tbCiclo();
                $('#modal-ciclo').modal('hide');
            }
        }
    });

}
function goCicloID(id) {
    //alert("Ver información del usuario : " + id).show();
}

function goEditCiclo(id) {
    $.post(url_ciclo.concat('ROWID'),
        { 'id_ciclo': id }, function (resp) {
            d = JSON.parse(resp);
            $("#id_ciclo").val(id);
            $("#nombre_ciclo").val(d.nombre_ciclo);
            $("#fecha_inicio").val(d.fecha_inicio);
            $("#fecha_fin").val(d.fecha_fin);
            $("#ciclo_actual").val(d.ciclo_actual)
            $('#modal-ciclo').modal({
                show: true,
                backdrop: 'static'
            });
        });
}
var tabla_ciclo;
function tbCiclo() {
    tabla_ciclo = $("#tb_ciclo").DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
        "ajax": {
            url: url_ciclo.concat('LIST'),
            type: "GET",
            dataType: "JSON",
            error: function (e) {
                console.log(e.responseText);
            },
        },
        "bDestroy": true,
        "iDisplayLength": 10,
        "order": [[1, "asc"]],
        "oLanguage": {
            "sProcessing": "Procesando...",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando del (_START_ al _END_) de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Por favor espere - cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            }
        }

    }).buttons().container().appendTo("#tb_ciclo_wrapper .col-md-6");
}

