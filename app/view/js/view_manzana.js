//* URL del controlador asociado a la vista
var url_manzana = 'app/controller/ManzanaController.php?ACTN=';

/**
 * *  initViewManzana: Es la primer funcón que se ejecuta cuando se
 * *  visualiza el modulo de Manzanas.
 */
function initViewManzana(){
    tbManzana();
    $("#form-manzana").on('submit', function(e){
        adManzanaNew(e);
    });
}

function addManzana() {
    cleanInputsManzana();
    
    $('#modal-manzana').modal({
        show: true,
        backdrop: 'static'
    });
}
function adManzanaNew(e) {
    e.preventDefault();
    var formData = new FormData($("#form-manzana")[0]);
    console.log(formData);
    $.ajax({
        url: url_manzana.concat('ADD'),
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data == "FAIL") {
                alert_rspt_error("Ocurrio un problema, vuelve a intentarlo");
            } else {
                $("#form-manzana")[0].reset();
                alert_rspt_success(data);
                tbManzana();
                $('#modal-manzana').modal('hide');
            }
        }
    });
}

function informeManzanaId(id) {
    $.post('app/view/pages/ViewInformeManzana.php', function(rspta){
        $("#panel_container").html(rspta);
        $("#page-title").html("");
        getInformationDetailManzanaID(id);
    });
}
function cleanInputsManzana() {
    $("#txt_idmanzana").val("");
    $("#txt_nombre_manzana").val("");
    $("#txt_descripcion_manzana").val("");
}
function goEditManzanaId(id) {
    $.post(url_manzana.concat('ROWID'),
        { 'txt_idmanzana': id }, function (resp) {
            console.log(resp);
            d = JSON.parse(resp);
            $("#txt_idmanzana").val(id);
            $("#txt_nombre_manzana").val(d.nombre_manzana);
            $("#txt_descripcion_manzana").val(d.des_manzana);
            $('#modal-manzana').modal({
                show: true,
                backdrop: 'static'
            });
        });
}


function deleteManzana(id){
    alert_question_delete().then((result) => {
        if (result.isConfirmed) {
            $.post(
                url_manzana.concat('DELETEID'),
                {'txt_idmanzana' : id},
                function(rspta){
                    console.log(id);
                    console.log(rspta);
                    if (rspta == "FAILD") {
                        alert_rspt_error("Ocurrio un problema, vuelve a intentarlo");
                    } else {
                        alert_rspt_success(rspta);
                        tbManzana();
                    }
                }
            );
        } 
    });;
}

function tbManzana() {
    $("#tb_manzana").DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "responsive": true,
        "ajax": {
            url: url_manzana.concat('LIST'),
            type: "GET",
            dataType: "JSON",
            error: function (e) {
                console.log(e.responseText);
            },
        },
        "bDestroy": true,
        "iDisplayLength": 10,
        "oLanguage": {
            "sProcessing": "Carganddo...",
            "sZeroRecords": "No se encontraron resultados para la manzana",
            "sEmptyTable": "Aún no se tienen registros sobre manzanas",
            "sInfo": "Mostrando del (_START_ al _END_) de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Por favor espere - cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            }
        }

    });

}

