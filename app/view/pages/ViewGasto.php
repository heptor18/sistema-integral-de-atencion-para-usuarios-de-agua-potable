<div class="card card-primary card-outline">
    <div class="card-body">
        <table id="tb_gasto" class="table table-sm table-striped table-bordered text-center">
            <thead class="table-primary">
                <tr>
                    <th> # </th>
                    <th> Persona-Comite </th>
                    <th> Fecha </th>
                    <th> Cantidad </th>
                    <th> Comentario </th>
                    <th> Nota </th>
                    <th> Opciones </th>
                </tr>
            </thead>
            <tbody class="table-primary table-light"></tbody>
        </table>
    </div>
</div>

<!---Modal - Gasto-->
<div class="modal fade" id="modal-gasto" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content card card-primary card-outline shadow">
            <div class="modal-header">
                <h4 class="modal-title text-center justify-content-between"><i class="fas fa-calendar-alt"></i> Gasto
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-gasto" action="#" method="POST">
                <div class="modal-body card-body">
                    <input id="id_gasto" name="id_gasto" type="hidden">
                    <div class="form-group">
                        <label for="nombre_ciclo">Fehca :</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-ca"></i></span>
                            </div>
                            <input type="text" id="nombre_ciclo" name="nombre_ciclo" class="form-control"
                                placeholder="Ingrese nombre" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="fecha_inicio">Inico:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-calendar"></i></span>
                            </div>
                            <input type="date" id="fecha_inicio" name="fecha_inicio" class="form-control"
                                 required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fecha_fin">Termina:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-calendar"></i></span>
                            </div>
                            <input type="date" id="fecha_fin" name="fecha_fin" class="form-control"
                               required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="icheck-primary">
                            <input class="mr-2" type="checkbox" id="ciclo_actual" name="ciclo_actual">
                            <label class="label-control" for="ciclo_actual">
                                Definir como ciclo actual
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!---Final Modal - Gasto-->
<script type="text/javascript" src="app/view/js/view_gasto.js"></script>