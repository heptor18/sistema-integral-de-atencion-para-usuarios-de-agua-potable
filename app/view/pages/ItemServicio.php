<div class="row">
    <div class="col-md-4">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Servicio</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <strong><i class="fas fa-book mr-1"></i> </strong>

                <p class="text-muted">
                    Servicio de toma de agua para Local.
                </p>

                <hr>

                <strong><i class="fas fa-map-marker-alt mr-1"></i> Dirección</strong>

                <p class="text-muted">Av. Reforma S/N</p>

                <hr>

                <strong><i class="fas fa-pencil-alt mr-1"></i> Fecha de Registro</strong>

                <p class="text-muted">2022-05-12</p>
                <hr>

                <strong><i class="far fa-file-alt mr-1"></i> Estado del servicio</strong>

                <p class="text-muted">Activo | Inactivo</p>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
    <!-- /.card -->
    <div class="col-md-8">
        <table id="tb_tarjeta" class="table table-sm table-striped table-bordered text-center">
            <thead class="table-primary">
                <tr>
                    <th> # </th>
                    <th> Mes </th>
                    <th> Folio </th>
                    <th> Fecha </th>

                </tr>
            </thead>
            <tbody class="table-primary table-light">
                <tr>
                    <td>1</td>
                    <td>Enero</td>
                    <td>1256</td>
                    <td>25-08-2022</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Febrero</td>
                    <td>1256</td>
                    <td>25-08-2022</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Marzo</td>
                    <td>1256</td>
                    <td>25-08-2022</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Abril</td>
                    <td>1256</td>
                    <td>25-08-2022</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Mayo</td>
                    <td>1256</td>
                    <td>25-08-2022</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Junio</td>
                    <td>1256</td>
                    <td>25-08-2022</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Julio</td>
                    <td>1256</td>
                    <td>25-08-2022</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Agosto</td>
                    <td>1256</td>
                    <td>25-08-2022</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Septiembre</td>
                    <td>1256</td>
                    <td>25-08-2022</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Octubre</td>
                    <td>1256</td>
                    <td>25-08-2022</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Noviembre</td>
                    <td>1256</td>
                    <td>25-08-2022</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Diciembre</td>
                    <td>1256</td>
                    <td>25-08-2022</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>