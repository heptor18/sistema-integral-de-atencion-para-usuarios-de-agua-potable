<div class="row">
    <div class="col-md-3">

        <!-- Profile Image -->
        <div class="card card-primary card-outline">
            <div class="card-body box-profile">
                <div class="text-center">
                    <img class="profile-user-img img-fluid img-circle" src="public/assets/img/img_person_blue_2.png"
                        alt="Perfil de usuario">
                </div>
                <input type="hidden" id="txt_idpersona">
                <h3 id="txt_nombre" class="profile-username text-center">---</h3>

                <p id="txt_direccion" class="text-muted text-center">---</p>

                <ul class="list-group list-group-unbordered mb-3">
                    <li class="list-group-item">
                        <b>Apellido Paterno</b> <a id="txt_ape_pat" class="float-right">---</a>
                    </li>
                    <li class="list-group-item">
                        <b>Apellido Materno</b> <a id="txt_ape_mat" class="float-right">---</a>
                    </li>
                    <li class="list-group-item">
                        <b>Manzana</b> <a id="txt_manzana" class="float-right">---</a>
                    </li>
                    <li class="list-group-item">
                        <b>Estado Civil</b> <a id="txt_edo_civil" class="float-right">---</a>
                    </li>
                    <li class="list-group-item">
                        <b>Pagado hasta el año</b> <a id="txt_fecha_pago" class="float-right">----</a>
                    </li>
                </ul>
                <a href="#" class="btn btn-primary btn-block"><b>Editar</b></a>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->


    </div>
    <!-- /.col -->
    <div class="col-md-9">
        <div class="card card-info">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Servicio</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <strong><i class="fas fa-book mr-1"></i> </strong>

                                <p class="text-muted" id="txt_servicio">
                                </p>

                                <hr>

                                <strong><i class="fas fa-map-marker-alt mr-1"></i> Dirección</strong>

                                <p class="text-muted" id="txt_direccion_servicio"></p>

                                <hr>

                                <strong><i class="fas fa-pencil-alt mr-1"></i> Fecha de Registro</strong>

                                <p class="text-muted" id="txt_alta_servicio"></p>
                                <hr>

                                <strong><i class="far fa-file-alt mr-1"></i> Estado del servicio</strong>

                                <p class="text-muted" id="txt_estatus_servicio"></p>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                    <!-- /.card -->
                    <div class="col-md-8">
                        <table id="tb_tarjeta" class="table table-sm table-striped table-bordered text-center">
                            <thead class="table-primary">
                                <tr>
                                    <th> # </th>
                                    <th> Mes </th>
                                    <th> Folio </th>
                                    <th> Fecha </th>

                                </tr>
                            </thead>
                            <tbody class="table-primary table-light">
                                <tr>
                                    <td>1</td>
                                    <td>Enero</td>
                                    <td>1256</td>
                                    <td>25-08-2022</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Febrero</td>
                                    <td>1256</td>
                                    <td>25-08-2022</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Marzo</td>
                                    <td>1256</td>
                                    <td>25-08-2022</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Abril</td>
                                    <td>1256</td>
                                    <td>25-08-2022</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Mayo</td>
                                    <td>1256</td>
                                    <td>25-08-2022</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Junio</td>
                                    <td>1256</td>
                                    <td>25-08-2022</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Julio</td>
                                    <td>1256</td>
                                    <td>25-08-2022</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Agosto</td>
                                    <td>1256</td>
                                    <td>25-08-2022</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Septiembre</td>
                                    <td>1256</td>
                                    <td>25-08-2022</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Octubre</td>
                                    <td>1256</td>
                                    <td>25-08-2022</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Noviembre</td>
                                    <td>1256</td>
                                    <td>25-08-2022</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Diciembre</td>
                                    <td>1256</td>
                                    <td>25-08-2022</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div><!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
<script type="text/javascript" src="app/view/js/view_perfil.js"></script>